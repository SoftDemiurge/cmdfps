﻿using System;
using System.Runtime.InteropServices;

/*
 * FileName: ConsoleAPI.cs
 * Author: Nikitin Dmitriy aka SoftDemiurge
 */

namespace CMDFPS
{
    internal static class ConsoleAPI
    {
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;
        public const int CONSOLE_TEXTMODE_BUFFER = 1;

        [DllImport("kernel32")]
        public static extern IntPtr CreateConsoleScreenBuffer(uint dwDesiredAccess, uint dwShareMode, IntPtr lpSecurityAttributes, uint dwFlags, IntPtr lpScreenBufferData);
        [DllImport("kernel32")]
        public static extern int SetConsoleActiveScreenBuffer(IntPtr hConsoleOutput);
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        public static extern bool WriteConsoleOutputCharacter(IntPtr hConsoleOutput, string lpCharacter, uint nLength, Coords dwWriteCoord, out uint lpNumberOfCharsWritten);
        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(System.Windows.Forms.Keys vKey);
    }
}
