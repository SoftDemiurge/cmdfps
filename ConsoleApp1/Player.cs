﻿using System;
/*
 * FileName: Player.cs
 * Author: Nikitin Dmitriy aka SoftDemiurge
 */

namespace CMDFPS
{
    public class Player
    {
        public float X;
        public float Y;
        public float Angle = 0.0f;
        public float FOV = (float)Math.PI / 4.0f;

        public Player(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
