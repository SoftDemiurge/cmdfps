﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * FileName: Config.cs
 * Author: Nikitin Dmitriy aka SoftDemiurge
 */

namespace CMDFPS
{

    public static class Config
    {
        public const float RotationSpeed = 0.8f;
        public const float MoveSpeed = 5.0f;

        public const char MiniMapWallChar = '#';
        public const char MiniMapPlayerChar = 'P';
        public const char BoundaryChar = ' ';


        public static char[] Walls = { '\u2588', '\u2593' , '\u2592', '\u2591', '\u0020' };
        public static char[] Floors = { MiniMapWallChar, 'x', '.', '-', BoundaryChar };

        public static Map Map;
        public static Player Player;
        public static Coords ScreenSize;
        public static uint BufferSize { get; private set; }

        public static void Initialize(short size, string map, short screenWidth, short screenHeigth)
        {
            Config.Map = new Map(size,map);
            ScreenSize = new Coords(screenWidth, screenHeigth);
            Player = new Player(size / 2, size / 2);
            BufferSize = (uint)(ScreenSize.X * ScreenSize.Y);
        } 
    }
}
