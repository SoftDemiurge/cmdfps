﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


//https://www.youtube.com/watch?v=xW8skO7MFYw


/*
 * Project: CMP FPS
 * Author: Nikitin Dmitriy aka SoftDemiurge
 */

namespace CMDFPS
{
    class Program
    {
        static StringBuilder screen;
        static IntPtr hConsole;
        static void Main(string[] args)
        {
            Config.Initialize(16, InitializeMap(), 120, 40);

            InitializeStringBuffer();
            InitializeScreenBuffer();


            Stopwatch lStopwatch = new Stopwatch();
            lStopwatch.Start();
            float tp1 = lStopwatch.ElapsedMilliseconds;
            float elapsedTime = 0;

            while (true)
            {
                float tp2 = lStopwatch.ElapsedMilliseconds;
                elapsedTime = (tp2 - tp1) / 1000;
                tp1 = tp2;


                if (IsButtonClicked(System.Windows.Forms.Keys.D))
                {
                    Rotate(elapsedTime, true);
                }

                if (IsButtonClicked(System.Windows.Forms.Keys.A))
                {
                    Rotate(elapsedTime);
                }

                if (IsButtonClicked(System.Windows.Forms.Keys.W))
                {
                    Move(elapsedTime, true);
                }

                if (IsButtonClicked(System.Windows.Forms.Keys.S))
                {
                    Move(elapsedTime);
                }

                if (IsButtonClicked(System.Windows.Forms.Keys.Q))
                {
                    SideMove(elapsedTime, true);
                }

                if (IsButtonClicked(System.Windows.Forms.Keys.E))
                {
                    SideMove(elapsedTime);
                }

                for (int x = 0; x < Config.ScreenSize.X; x++)
                {
                    float fRayAngle = (Config.Player.Angle - Config.Player.FOV / 2.0f) + ((float)x / (float)Config.ScreenSize.X) *  Config.Player.FOV;
                    float fDistanceToWall = 0;

                    bool bHitWall = false;
                    bool bBoundary = false;

                    float fEyeX = (float)Math.Sin(fRayAngle);
                    float fEyeY = (float)Math.Cos(fRayAngle);

                    while (!bHitWall && fDistanceToWall < Config.Map.Depth)
                    {
                        fDistanceToWall += 0.1f;

                        int nTestX = (int)(Config.Player.X + fEyeX * fDistanceToWall);
                        int nTestY = (int)(Config.Player.Y + fEyeY * fDistanceToWall);

                        if (nTestX < 0 || nTestX >= Config.Map.Width || nTestY < 0 || nTestY >= Config.Map.Heigth)
                        {
                            bHitWall = true;
                            fDistanceToWall = Config.Map.Depth;
                        }
                        else
                        {
                            if (Config.Map[nTestY * Config.Map.Width + nTestX] == Config.MiniMapWallChar)
                            {
                                bHitWall = true;

                                List<KeyValuePair<float, float>> p = new List<KeyValuePair<float, float>>();
                                for (int tx = 0; tx < 2; tx++)
                                {
                                    for (int ty = 0; ty < 2; ty++)
                                    {
                                        float vy = (float)nTestY + ty - Config.Player.Y;
                                        float vx = (float)nTestX + tx - Config.Player.X;
                                        float d = (float)Math.Sqrt(vx * vx + vy * vy);
                                        float dot = (fEyeX * vx / d) + (fEyeY * vy / d);
                                        p.Add(new KeyValuePair<float, float>(d, dot));
                                    }
                                }

                                p = p.OrderBy(g => g.Key).ToList();

                                float fBound = 0.01f;
                                if (Math.Acos(p.ElementAt(0).Value) < fBound)
                                {
                                    bBoundary = true;
                                }
                                if (Math.Acos(p.ElementAt(1).Value) < fBound)
                                {
                                    bBoundary = true;
                                }
                                /*  if (Math.Acos(p.ElementAt(2).Value) < fBound)
                                  {
                                      bBoundary = true;
                                  }*/
                            }
                        }
                    }

                    int nCelling = (int)((float)(Config.ScreenSize.Y / 2.0f) - Config.ScreenSize.Y / ((float)fDistanceToWall));
                    int nFloor = Config.ScreenSize.Y - nCelling;


                    char nShade = Config.Walls[4];
                    for (int y = 0; y < Config.ScreenSize.Y; y++)
                    {
                        if (y < nCelling)
                        {
                            screen[y * Config.ScreenSize.X + x] = Config.Walls[4];
                        }
                        else if (y > nCelling && y <= nFloor)
                        {

                            if (fDistanceToWall <= Config.Map.Depth / 4.0f)
                            {
                                nShade = Config.Walls[0];
                            }
                            else if (fDistanceToWall <= Config.Map.Depth / 3.0f)
                            {
                                nShade = Config.Walls[1];
                            }
                            else if (fDistanceToWall <= Config.Map.Depth / 2.0f)
                            {
                                nShade = Config.Walls[2];
                            }
                            else if (fDistanceToWall <= Config.Map.Depth)
                            {
                                nShade = Config.Walls[3];
                            }
                            else
                            {
                                nShade = Config.Walls[4];
                            }

                            if (bBoundary)
                            {
                                nShade = Config.BoundaryChar;
                            }

                            screen[y * Config.ScreenSize.X + x] = nShade;
                        }
                        else
                        {
                            float b = 1.0f - (((float)y - Config.ScreenSize.Y / 2.0f) / ((float)Config.ScreenSize.Y / 2.0f));
                            if (b < 0.25)
                            {
                                nShade = Config.Floors[0];
                            }
                            else if (b < 0.25)
                            {
                                nShade = Config.Floors[1];
                            }
                            else if (b < 0.5)
                            {
                                nShade = Config.Floors[2];
                            }
                            else if (b < 0.75)
                            {
                                nShade = Config.Floors[3];
                            }
                            else
                            {
                                nShade = Config.Floors[4];
                            }
                            screen[y * Config.ScreenSize.X + x] = nShade;
                        }
                    }
                }

                ShowStats(elapsedTime);
                DrawMiniMap();
                Draw();
            }
        }
        private static bool IsButtonClicked(System.Windows.Forms.Keys key)
        {
            return 0 != (ConsoleAPI.GetAsyncKeyState(key) & 0x8000);
        }

        private static string InitializeMap()
        {
            StringBuilder map = new StringBuilder();
            map.Append("################");
            map.Append("#..............#");
            map.Append("#..............#");
            map.Append("#..............#");
            map.Append("#...........#..#");
            map.Append("#...........#..#");
            map.Append("#..............#");
            map.Append("#..............#");
            map.Append("#..............#");
            map.Append("#..............#");
            map.Append("#..............#");
            map.Append("#..............#");
            map.Append("#......#########");
            map.Append("#..............#");
            map.Append("#..............#");
            map.Append("################");
            return map.ToString();
        }

        private static void InitializeStringBuffer()
        {
            screen = new StringBuilder((int)Config.BufferSize);
            for (int h = 0; h < Config.BufferSize; h++)
            {
                screen.Append(' ');
            }
        }

        private static void InitializeScreenBuffer()
        {
            hConsole = ConsoleAPI.CreateConsoleScreenBuffer(ConsoleAPI.GENERIC_READ | ConsoleAPI.GENERIC_WRITE, 0, IntPtr.Zero, ConsoleAPI.CONSOLE_TEXTMODE_BUFFER, IntPtr.Zero);
            ConsoleAPI.SetConsoleActiveScreenBuffer(hConsole);
        }

        private static void Draw()
        {
            screen[Config.ScreenSize.Y * Config.ScreenSize.X - 1] = '\0';
            uint dwBytesWritten = 0;
            ConsoleAPI.WriteConsoleOutputCharacter(hConsole, screen.ToString(), Config.BufferSize, new Coords(0, 0), out dwBytesWritten);
        }

        private static void Rotate(float elTime, bool isReverse = false)
        {
            Config.Player.Angle += (isReverse ? 1 : -1) * Config.RotationSpeed * elTime;
        }

        private static void Move(float elTime, bool isReverse = false)
        {
            Config.Player.X += (isReverse ? 1 : -1) * (float)Math.Sin(Config.Player.Angle) * Config.MoveSpeed * elTime;
            Config.Player.Y += (isReverse ? 1 : -1) * (float)Math.Cos(Config.Player.Angle) * Config.MoveSpeed * elTime;

            if (Config.Map[(int)Config.Player.Y * Config.Map.Width + (int)Config.Player.X] == Config.MiniMapWallChar)
            {
                Config.Player.X += (isReverse ? -1 : 1) * (float)Math.Sin(Config.Player.Angle) * Config.MoveSpeed * elTime;
                Config.Player.Y += (isReverse ? -1 : 1) * (float)Math.Cos(Config.Player.Angle) * Config.MoveSpeed * elTime;
            }
        }

        private static void SideMove(float elTime, bool isReverse = false)
        {
            Config.Player.Y += (isReverse ? 1 : -1) * (float)Math.Sin(Config.Player.Angle) * Config.MoveSpeed * elTime;
            Config.Player.X += (isReverse ? 1 : -1) * (float)Math.Cos(Config.Player.Angle) * Config.MoveSpeed * elTime;

            if (Config.Map[(int)Config.Player.Y * Config.Map.Width + (int)Config.Player.X] == Config.MiniMapWallChar)
            {
                Config.Player.Y += (isReverse ? -1 : 1) * (float)Math.Sin(Config.Player.Angle) * Config.MoveSpeed * elTime;
                Config.Player.X += (isReverse ? -1 : 1) * (float)Math.Cos(Config.Player.Angle) * Config.MoveSpeed * elTime;
            }
        }

        private static void ShowStats(float elTime)
        {
            string stats = String.Format("X={0}, Y={1}, A={2}, FPS={3}", Config.Player.X, Config.Player.Y, Config.Player.Angle, (int)(1.0f / elTime));
            screen.Insert(0, stats);
            screen.Remove(stats.Length - 1, stats.Length);
        }

        private static void DrawMiniMap()
        {
            for (int nx = 0; nx < Config.Map.Width; nx++)
            {
                for (int ny = 0; ny < Config.Map.Width; ny++)
                {
                    screen[(ny + 1) * Config.ScreenSize.X + nx] = Config.Map[ny * Config.Map.Width + nx];
                }
            }

            screen[((int)Config.Player.Y + 1) * Config.ScreenSize.X + (int)Config.Player.X] = Config.MiniMapPlayerChar;
        }
    }
}