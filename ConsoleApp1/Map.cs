﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * FileName: Map.cs
 * Author: Nikitin Dmitriy aka SoftDemiurge
 */

namespace CMDFPS
{
    public class Map
    {
        public int Heigth;
        public int Width;
        public readonly float Depth;
        private string map;

        public Map(int heigth, int width, string map)
        {
            Heigth = heigth;
            Width = width;
            Depth = Math.Max(Heigth, Width);
            this.map = map;
        }

        public Map(int size, string map) : this(size,size,map)
        {

        }

        public char this[int index]
        {
            get
            {
                return map[index];
            }
        }
    }
}
