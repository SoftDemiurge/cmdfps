﻿using System.Runtime.InteropServices;

/*
 * FileName: Coords.cs
 * Author: Nikitin Dmitriy aka SoftDemiurge
 */

namespace CMDFPS
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Coords
    {
        public short X;
        public short Y;

        public Coords(short X, short Y)
        {
            this.X = X;
            this.Y = Y;
        }
    };
}
